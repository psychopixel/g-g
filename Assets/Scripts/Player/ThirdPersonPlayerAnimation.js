var runSpeedScale = 1.0;
var walkSpeedScale = 1.0;
var isClimbing = false;

private var arthur:Transform;

function Start ()
{
	setAnimation();
}
function setAnimation() {
	arthur = GetComponent("ThirdPersonStatus").arthur.transform;
	// By default loop all animations
	arthur.animation.wrapMode = WrapMode.Loop;

	arthur.animation["run"].layer = -1;
	arthur.animation["walk"].layer = -1;
	arthur.animation.SyncLayer(-1);
	arthur.animation["idle"].layer = -2;
	
	arthur.animation["jump"].layer = 2;
	arthur.animation["jumpfall"].layer = 2;
	arthur.animation.SyncLayer(2);
	arthur.animation["crouch"].layer = 3;
	arthur.animation["shot"].layer = 4;

	// The jump animation is clamped and overrides all others
	
	arthur.animation["jump"].wrapMode = WrapMode.ClampForever;

	arthur.animation["jumpfall"].wrapMode = WrapMode.ClampForever;
	
	arthur.animation["crouch"].wrapMode = WrapMode.ClampForever;
	
	arthur.animation["shot"].blendMode = AnimationBlendMode.Blend;
	arthur.animation["shot"].wrapMode = WrapMode.Once;

	// We are in full control here - don't let any other animations play when we start
	arthur.animation.Stop();
	arthur.animation.Play("idle");
}
function Update ()
{
	var playerController : ThirdPersonController = GameObject.Find("Player").GetComponent(ThirdPersonController);
	var currentSpeed = playerController.GetSpeed();
	// Fade in run
	if (currentSpeed > 0.1)
	{
		if(!isClimbing) {
			arthur.animation.CrossFade("run");
			// We fade out jumpland realy quick otherwise we get sliding feet
			arthur.animation.Blend("jumpfall", 0);
		}
	}
	// Fade out walk and run
	else
	{
		if(!isClimbing) {
			arthur.animation.Blend("run", 0.0, 0.3);
			arthur.animation.Blend("run", 0.0, 0.3);
			arthur.animation.Play("idle");
		}
	}
	if(!isClimbing) {
		arthur.animation["run"].normalizedSpeed = runSpeedScale;
	}
	if (playerController.IsJumping ())
	{
		if (playerController.HasJumpReachedApex ())
		{
			arthur.animation.CrossFade ("jumpfall", 0.2);
		}
		else
		{
			arthur.animation.CrossFade ("jump", 0.2);
		}
	}
	// We fell down somewhere
	else if (!playerController.IsGroundedWithTimeout())
	{
		if(!isClimbing) {
			arthur.animation.CrossFade ("idle", 0.2);
		}
	}
	// We are not falling down anymore
	else
	{
		if(!isClimbing) {
			arthur.animation.Blend("jumpfall", 0.0, 0.3);
			arthur.animation.Blend ("idle",1.0, 0.2);
		}
	}
}

function StartClimb() {
	arthur.animation.Play("climb",PlayMode.StopAll);
	isClimbing = true;
}

function PauseClimb() {
	arthur.animation.Stop("climb");
}

function StopClimb() {
	arthur.animation.Blend("climb", 0.0, 0.2);
	arthur.animation.Blend ("idle",1.0, 0.2);
	isClimbing = false;
}

function DidLand () {
	arthur.animation.Play("idle");
}

function Crouch () {
	Debug.Log ("Crouch");
	arthur.animation.Play("crouch",PlayMode.StopAll);
}

function ShotJumping() {
	Debug.Log ("ShotJumping");
	// Wait for the animation to have finished
	arthur.animation.Blend("shot",1,0.0);
	yield WaitForSeconds (arthur.animation.clip.length);
	arthur.animation.Blend("shot",0,0.1);
}

function ShotUp() {
	Debug.Log ("ShotUp");
	// Wait for the animation to have finished
	arthur.animation.Blend("shot",1,0.0);
	yield WaitForSeconds (arthur.animation.clip.length);
	arthur.animation.Blend("shot",0,0.0);
}

function ShotDown () {
	Debug.Log ("ShotDown");
	// Wait for the animation to have finished
	arthur.animation.Blend("shot",1,0.0);
	yield WaitForSeconds (arthur.animation.clip.length);
	arthur.animation.Blend("shot",0,0.0);
}

function Rise () {
	arthur.animation.Blend("crouch", 0.0, 0.2);
	arthur.animation.Blend ("idle",1.0, 0.2);
}


@script AddComponentMenu ("Third Person Player/Third Person Player Animation")