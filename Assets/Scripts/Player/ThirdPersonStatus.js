// ThirdPersonStatus: Handles the player's state machine.
// Keeps track of inventory, health, lives, etc.

var health : int 		= 2;
var maxHealth : int 	= 2;
var lives : int 			= 3;
var points : int 		= 0;
var pointsPerLife:float = 5000;
var lifeMinutes:int	= 5;
private var pointsPerSecond:float = (pointsPerLife/lifeMinutes)/60;

var lifeStartedTime:int;
var isInvulnerable:boolean = false;

private var arthur:GameObject = null;
var arthurNude:GameObject;
var arthurCorazza:GameObject;

// sound effects.
var deathSound: AudioClip;
var damageSound: AudioClip;

var standardMaterial:Material;

private var levelStateMachine : LevelStatus;		// link to script that handles the level-complete sequence.


function Awake()
{
	if(arthur == null) {
		arthur = Instantiate(arthurCorazza, transform.position, transform.rotation);
		arthur.transform.parent = transform;
		GetComponent("ThirdPersonPlayerAnimation").setAnimation();
	}
	
	lifeStartedTime = Time.time;
	levelStateMachine = FindObjectOfType(LevelStatus);
	if (!levelStateMachine)
		Debug.Log("No link to Level Status");
	
}

function ApplyDamage (damage : int)
{
	if(isInvulnerable) {
	
	} else {
		if (damageSound)
			AudioSource.PlayClipAtPoint(damageSound, transform.position);	// play the 'player was damaged' sound.

		health -= damage;
		if(health == 1) {
			Destroy(arthur);
			arthur = Instantiate(arthurNude, transform.position, transform.rotation);
			arthur.transform.parent = transform;
			GetComponent("ThirdPersonPlayerAnimation").setAnimation();
			isInvulnerable = true;
			yield WaitForSeconds(2);
			isInvulnerable = false;
			var myRenderer : Renderer;
			myRenderer = arthur.GetComponentInChildren(Renderer);
			myRenderer.material = standardMaterial;
			Debug.Log(myRenderer.material);
		}
		
		if (health <= 0)
		{
			SendMessage("Die");
		}
	}
}


function AddLife (powerUp : int)
{

	lives += powerUp;
	health = maxHealth;

}

function AddHealth (powerUp : int)
{

	health += powerUp;
	
	if (health>maxHealth)	{
		health=maxHealth;	
	}

}

function AddBonusPoints() {
	AddPoints(lives * pointsPerLife);
	var currentLifeTime:int = Time.time - lifeStartedTime;
	var remaingTime:int = lifeMinutes * 60 - currentLifeTime;
	if(health==1) {
		AddPoints(remaingTime * pointsPerSecond * 0.5);
	} else {
		AddPoints(remaingTime * pointsPerSecond * 0.5);
	}
}

function AddPoints (newPoints : int)
{

	points += newPoints;

}

function FalloutDeath ()
{

	Die();
	return;
}

function Die ()
{
	Debug.Log("Die");
	// play the death sound if available.
	if (deathSound)
	{
		AudioSource.PlayClipAtPoint(deathSound, transform.position);

	}
		
	lives--;
	health = maxHealth;
	Destroy(arthur);
	if(lives < 0)
		Application.LoadLevel("GameOver");	
	
	// If we've reached here, the player still has lives remaining, so respawn.
	respawnPosition = Respawn.currentRespawn.transform.position;
	// Relocate the player. We need to do this or the camera will keep trying to focus on the (invisible) player where he's standing on top of the FalloutDeath box collider.
	transform.position = respawnPosition;
	transform.eulerAngles = Vector3(0.0, 270.0, 0.0);
	arthur = Instantiate(arthurCorazza, respawnPosition, transform.rotation);
	arthur.transform.parent = transform;
	GetComponent("ThirdPersonPlayerAnimation").setAnimation();
	// Hide the player briefly to give the death sound time to finish...
	// (NOTE: "HidePlayer" also disables the player controls.)
	SendMessage("HidePlayer");
	
	Camera.main.transform.position = respawnPosition - (transform.forward * 4) + Vector3.up;	// reset camera too
		
	yield WaitForSeconds(1.6);	// give the sound time to complete. 
	
	lifeStartedTime = Time.time;
	SendMessage("ShowPlayer");	// Show the player again, ready for...	
	// ... the respawn point to play it's particle effect
	Respawn.currentRespawn.FireEffect ();
}

function EndLevel() {
	levelStateMachine.endLevelPoints = points;
	Application.LoadLevel("EndLevel1");
}