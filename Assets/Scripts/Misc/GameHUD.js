// GameHUD
// This script handles the in-game HUD, showing the lives, number of points, etc.

var guiSkin: GUISkin;
var nativeVerticalResolution = 768.0;

// main decoration textures:
var livesImage: Texture2D;
var livesImageOffset = Vector2(0, 0);

// the points count is displayed as a text counter
var pointsCountOffset = Vector2(400, 30);

// the life time is displayed as a text counter
private var lifeTimeOffset = Vector2(260, 80);

private var remaingTime:int;
private var remaingTimeString:String;
private var currentLifeTime:int;

private var playerInfo : ThirdPersonStatus;

// Cache link to player's state management script for later use.
function Awake()
{
	playerInfo = FindObjectOfType(ThirdPersonStatus);

	if (!playerInfo)
		Debug.Log("No link to player's state manager.");
}

function OnGUI ()
{

	
	// Set up gui skin
	GUI.skin = guiSkin;

	// Our GUI is laid out for a 1024x 768 pixel display (4:3 aspect). The next line makes sure it rescales nicely to other resolutions.
	GUI.matrix = Matrix4x4.TRS (Vector3(0, 0, 0), Quaternion.identity, Vector3 (Screen.height / nativeVerticalResolution, Screen.height / nativeVerticalResolution, 1)); 
	
	GUI.BeginGroup (Rect(0, 0, 1024, 768));
	
	// Displays points left as a number.
	GUI.Label(Rect (pointsCountOffset.x, pointsCountOffset.y, 100, 100), "punti: "+ playerInfo.points.ToString());
	// Displays life time left as a number.
	GUI.Label(Rect (lifeTimeOffset.x, lifeTimeOffset.y, 100, 100), "time: "+ remaingTimeString);
	// Health & lives info.
	for(var i = 0; i<playerInfo.lives; i++) {
		DrawImageBottomAligned( livesImageOffset+Vector2(i*90,0), livesImage); // main image.
	}
	
	GUI.EndGroup ();
}

function Update() {
	currentLifeTime = Time.time - playerInfo.lifeStartedTime;
	remaingTime = playerInfo.lifeMinutes * 60 - currentLifeTime;
	var minute:int = Mathf.Ceil(remaingTime/60);
	var seconds:int = remaingTime%60;
	var minuteString:String;
	if(minute==0) {
		minuteString="00";
		} else if (minute<10) {
			minuteString="0"+minute.ToString();
			} else {
				minuteString=minute.ToString();
			}
	var secondString:String;
	if(seconds==0) {
		secondString="00";
		} else if (seconds<10) {
			secondString="0"+seconds.ToString();
			} else {
				secondString=seconds.ToString();
			}
	remaingTimeString = (minuteString+":"+secondString);
	if(remaingTime<=0) {
		remaingTime = 0;
		playerInfo.Die();
	}
	
}

function DrawImageBottomAligned (pos : Vector2, image : Texture2D)
{
	GUI.Label(Rect (pos.x, nativeVerticalResolution - image.height - pos.y, image.width, image.height), image);
}

function DrawLabelBottomAligned (pos : Vector2, text : String)
{
	GUI.Label(Rect (pos.x, nativeVerticalResolution - pos.y, 100, 100), text);
}

function DrawImageBottomRightAligned (pos : Vector2, image : Texture2D)
{
	var scaledResolutionWidth = nativeVerticalResolution / Screen.height * Screen.width;
	GUI.Label(Rect (scaledResolutionWidth - pos.x - image.width, nativeVerticalResolution - image.height - pos.y, image.width, image.height), image);
}

function DrawLabelBottomRightAligned (pos : Vector2, text : String)
{
	var scaledResolutionWidth = nativeVerticalResolution / Screen.height * Screen.width;
	GUI.Label(Rect (scaledResolutionWidth - pos.x, nativeVerticalResolution - pos.y, 100, 100), text);
}