// LevelStatus: Master level state machine script.


// This is where info like the number of items the player must collect in order to complete the level lives.

var currentLevelNumber: int 	= 1;	// This is the current level number.

var endLevelPoints:int = 0;

// Make this game object and all its transform children
// survive when loading a new scene.
function Awake () {
	DontDestroyOnLoad (this);
}

function OnLevelWasLoaded () {
	if(Application.loadedLevelName=="GameOver" || Application.loadedLevelName=="EndLevel1") {
		Destroy(this.GetComponent("GameHUD"));
	}
}