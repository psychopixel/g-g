var levelToLoad : String;
var normalTexture : Texture2D;
var rollOverTexture : Texture2D;
var beep : AudioClip;
var quitButton : boolean = false;
var playAgainButton : boolean = false;

function OnMouseEnter() {
	guiTexture.texture = rollOverTexture;
}

function OnMouseExit() {
	guiTexture.texture = normalTexture;
}

function OnMouseUp() {
	audio.PlayOneShot(beep);
	yield new WaitForSeconds(0.35);
	if(quitButton) {
		Application.Quit();
	} else {
		if(playAgainButton) {
			resetGame();
		}
		Application.LoadLevel(levelToLoad);
	}
}

function resetGame() {
	Debug.Log("resetGame");
}

@script RequireComponent(AudioSource)