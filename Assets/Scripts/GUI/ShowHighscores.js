var highScoresURL= "http://www.imageinaction.net/works/g&g/g&g_highscores.php";
var playerName;
var scores;
var highscores:Array;
var result : boolean;
var showHighscore : boolean = false;
var highscoresWindowRect : Rect;
var highscoresWindowDone:boolean = false;
var highscoresWindowStyle:GUIStyle;
var scoresWindowStyle:GUIStyle;
var backBtnStyle:GUIStyle;
function Start() {
	highscoresWindowRect.x = 22;
	highscoresWindowRect.y = 19;
	highscoresWindowRect.width = 980;
	highscoresWindowRect.height = 730;
	
	
	var xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?><data result=\"true\"><scores id=\"1\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"2\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"3\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"4\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"5\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"6\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"7\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"8\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"9\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"10\" username=\"Arthur\" score=\"1000\"></scores></data>";
	parseXml(xml);
/*
	// Create a form object for sending high score data to the server
	var form = new WWWForm();
	// set the type of the request (get or set)
	form.AddField( "requestType", "get" );

	// Create a download object
	var download = new WWW( highScoresURL, form );

	// Wait until the download is done
	yield download;

	if(download.error) {
		print( "Error downloading: " + download.error );
		return;
	} else {
		// show the highscores
		Debug.Log(download.text);
		parseXml(download.text);
	}
	*/
	
}

function parseXml(xml:String) {
	var parser=new XMLParser();
	var node=parser.Parse(xml);
	result =(node["data"][0]["@result"]=="true"?true:false);
	if(result) {
		Debug.Log("result = true");
		var elemLength:int = node["data"][0]["scores"].length;
		highscores = new Array();
		for(var i:int=0; i< elemLength; i++) {
			var obj:Hashtable = new Hashtable();
			obj.Add("id", node["data"][0]["scores"][i]["@id"]);
			obj.Add("username",  node["data"][0]["scores"][i]["@username"]);
			obj.Add("score", node["data"][0]["scores"][i]["@score"]);
			highscores.push(obj);
		}
		showHighscore = true;
	} else {
		Debug.Log("result = false");
	}
}
function OnGUI() {
	showHighscoresData();
}
function showHighscoresData() {
	if(showHighscore) {
		Debug.Log("showHighscoresData() ");
		highscoresWindowRect = GUI.Window (0, highscoresWindowRect, DoHighscoresWindow, "", highscoresWindowStyle);
	}
}
// Make the contents of the window.
function DoHighscoresWindow (windowID : int) {
	var newWindowId:int = 1;
	for(var j:int=0; j< 10; j++) {
		var newRect = new Rect(250, (37 * (j+1)) +115, 600, 50);
		GUI.Label(newRect,(highscores[j]["username"]),scoresWindowStyle);
		newRect.x += 380;
		GUI.Label(newRect,(highscores[j]["score"]),scoresWindowStyle);
	}
	if(GUI.Button(Rect(20,680,64,48), "", backBtnStyle)) {
		Application.LoadLevel("MainMenu");
	}
}
function DoNewRectWindow(windowID : int) {
	
}

function Update () {
}