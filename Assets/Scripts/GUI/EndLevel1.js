var highScoresURL= "http://www.imageinaction.net/works/g&g/g&g_highscores.php";
var playerName:String = "";
var highscores:Array;
var result : boolean;
var saveHighscoresWindowRect : Rect;
var saveHighscoresWindowDone:boolean = false;
var saveHighscoresWindowStyle:GUIStyle;
var scoresWindowStyle:GUIStyle;
var scoresInputStyle:GUIStyle;
var cancelBtnStyle:GUIStyle;
var saveBtnStyle:GUIStyle;
var playAgainBtnStyle:GUIStyle;
var menuBtnStyle:GUIStyle;
var highScoreDone:boolean = false;

private var levelStateMachine : LevelStatus;		// link to script that handles the level-complete sequence.

// Cache link to player's state management script for later use.
function Awake()
{
	levelStateMachine = FindObjectOfType(LevelStatus);
	if (!levelStateMachine)
		Debug.Log("No link to Level Status");
}

function Start() {
	playerName = "";
	saveHighscoresWindowRect.x = 53;
	saveHighscoresWindowRect.y = 224;
	saveHighscoresWindowRect.width = 918;
	saveHighscoresWindowRect.height = 320;
	
	/*
	// just to test locally
	var xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?><data result=\"true\"><scores id=\"1\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"2\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"3\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"4\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"5\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"6\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"7\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"8\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"9\" username=\"Arthur\" score=\"1000\"></scores><scores id=\"10\" username=\"Arthur\" score=\"1000\"></scores></data>";
	parseXml(xml);
	*/

	// Create a form object for sending high score data to the server
	var form = new WWWForm();
	// set the type of the request (get or set)
	form.AddField( "requestType", "get" );

	// Create a download object
	var download = new WWW( highScoresURL, form );

	// Wait until the download is done
	yield download;

	if(download.error) {
		print( "Error downloading: " + download.error );
		return;
	} else {
		// show the highscores
		Debug.Log(download.text);
		parseXml(download.text);
	}
	
}

function parseXml(xml:String) {
	var parser=new XMLParser();
	var node=parser.Parse(xml);
	result =(node["data"][0]["@result"]=="true"?true:false);
	if(result) {
		Debug.Log("result = true");
		var elemLength:int = node["data"][0]["scores"].length;
		highscores = new Array();
		highScoreDone = false;
		for(var i:int=0; i< elemLength; i++) {
			var obj:Hashtable = new Hashtable();
			obj.Add("id", node["data"][0]["scores"][i]["@id"]);
			obj.Add("username",  node["data"][0]["scores"][i]["@username"]);
			obj.Add("score", node["data"][0]["scores"][i]["@score"]);
			highscores.push(obj);
		}
		for(var k:int = 9; k>=0; k--) {
			var pointsAsNumber:int = parseInt (highscores[k]["score"] );
			if(levelStateMachine.endLevelPoints > pointsAsNumber|| (levelStateMachine.endLevelPoints == pointsAsNumber && k < 9) ) {
				highScoreDone = true;
				break;
			}
		}
		Debug.Log("points: "+ levelStateMachine.endLevelPoints+" highScoreDone:"+highScoreDone);
	} else {
		Debug.Log("result = false");
	}
}

function OnGUI() {
	if(highScoreDone) {
		showHighscoresDataInput();
	} else {
		if(GUI.Button(Rect((Screen.width-306)/2,250,306,112), "", playAgainBtnStyle)) {
			Application.LoadLevel("level1");
		}
		if(GUI.Button(Rect((Screen.width-362)/2,350,362,112), "", menuBtnStyle)) {
			Application.LoadLevel("MainMenu");
		}
	}
}

function showHighscoresDataInput() {
	Debug.Log("showHighscoresDataInput() ");
	saveHighscoresWindowRect = GUI.Window (0, saveHighscoresWindowRect, DoSaveHighscoresWindow, "", saveHighscoresWindowStyle);
}

// Make the contents of the window.
function DoSaveHighscoresWindow (windowID : int) {
	GUI.SetNextControlName ("username");
	playerName = GUI.TextField(Rect(295,190,330,40), playerName, 20, scoresInputStyle);
	GUI.FocusControl ("username");
	if(GUI.Button(Rect(265,250,124,77), "", saveBtnStyle)) {
		SaveHighscore();
	}
	if(GUI.Button(Rect(515,250,154,81), "", cancelBtnStyle)) {
		Application.LoadLevel("MainMenu");
	}
}
function SaveHighscore() {
	var form = new WWWForm();
	// set the type of the request (get or set)
	form.AddField( "requestType", "set" );
	form.AddField("username", playerName);
	form.AddField("score", levelStateMachine.endLevelPoints);
		// Create a download object
	var download = new WWW( highScoresURL, form );

	// Wait until the download is done
	yield download;

	if(download.error) {
		print( "Error downloading: " + download.error );
		return;
	} else {
		// show the highscores
		Application.LoadLevel("ShowHighscores");
	}
}
function DoNewRectWindow(windowID : int) {
	
}

function Update () {
}