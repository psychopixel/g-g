var target : Transform;
var verticalOffset:float = 2.0;

private var controller : ThirdPersonController;
private var velocity = Vector3.zero;
private var initialPosition = Vector3.zero;

function Awake ()
{
	initialPosition = transform.position;
	
	if (target)
	{
		controller = target.GetComponent(ThirdPersonController);
	}
	
	if (!controller)
		Debug.Log("Please assign a target to the camera that has a Third Person Controller script component.");
		
	if(controller) {
		updatePosition ();
	}
}

function Update () {
	updatePosition ();
}

function updatePosition () {
	var newY :float;
	if(target.position.y+verticalOffset>initialPosition.y) {
		newY = target.position.y+verticalOffset;
	} else {
		newY = initialPosition.y;
	}
	var newPosition: Vector3 = new Vector3(target.position.x, newY, initialPosition.z);
	transform.position = newPosition;
}