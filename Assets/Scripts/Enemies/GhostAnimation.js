var bonusPoints = 100;
var respawner:GhostRespawner;
var target:Transform;

var pathToFollow:GameObject;

// The speed when flying
var flySpeed:float 						= 3;
var rotateSpeed 							= 50.0;

private var DEACTIVATED:String				= "DEACTIVATED";
private var FLYING:String				 			= "FLYING";
private var DYING:String 							= "DYING";

private var waypoints : Array					= new Array();
private var currentWaypoint : int = 0;

var oldStatus:String			= DEACTIVATED;
var currentStatus:String 	= FLYING;

var maxHealth:int = 1;
var health:int;

// The gravity for the character
var gravity = 0.0;

// The current x-z move speed
private var moveSpeed = flySpeed;

function Awake () {
	if(!target) {
		target = GameObject.Find("Player").transform;
	}
}


function Start ()
{
	// By default loop all animations
	animation.wrapMode = WrapMode.Loop;

	animation["fly"].layer = -1;
	animation.SyncLayer(-1);
	
	// We are in full control here - don't let any other animations play when we start
	animation.Stop();
	animation.Play("fly");
	health = maxHealth;
	
	GetWaypoints();
}

function GetWaypoints() {
	var potentialWaypoints:Array = pathToFollow.GetComponentsInChildren( Transform );
	waypoints = new Array();
	Debug.Log("potentialWaypoints.length:"+potentialWaypoints.length);
	for (var potentialWaypoint :Transform in potentialWaypoints) {
		if(potentialWaypoint != pathToFollow.transform) {
			waypoints[waypoints.length] = potentialWaypoint;
		}
	}
	Debug.Log("waypoints.length:"+waypoints.length);
}
function rotateToTarget(obj:Transform) {
	transform.LookAt(obj.position);
	angle = RotateTowardsPosition(obj.position, rotateSpeed);
	angle = Mathf.Abs(RotateTowardsPosition(obj.position, rotateSpeed));
	move = Mathf.Clamp01((90 - angle) / 90);
	direction = transform.TransformDirection(Vector3.forward * move);
	return direction;
}
function RotateTowardsPosition (targetPos : Vector3, rotateSpeed : float) : float
{
	// Compute relative point and get the angle towards it
	var relative = transform.InverseTransformPoint(targetPos);
	var angle = Mathf.Atan2 (relative.x, relative.z) * Mathf.Rad2Deg;
	// Clamp it with the max rotation speed
	var maxRotation = rotateSpeed * Time.deltaTime;
	var clampedAngle = Mathf.Clamp(angle, -maxRotation, maxRotation);
	// Rotate
	transform.Rotate(0, clampedAngle, 0);
	// Return the current angle
	return angle;
}
function Follow(obj:Transform) {
	direction = rotateToTarget(obj);
	var controller : CharacterController = GetComponent(CharacterController);
	controller.Move(direction * flySpeed * Time.deltaTime);
}
function NavigateTowardsWaypoint() {
	if(waypoints.length>0) {
		Debug.Log("waypoints[currentWaypoint].position:"+waypoints[currentWaypoint].position);
		Follow(waypoints[currentWaypoint]);
		var dist:float = Mathf.Abs(Vector3.Distance(waypoints[currentWaypoint].position, transform.position));
		Debug.Log("dist:"+dist);
		if(dist < 2) {
			currentWaypoint++;
		}
		if(currentWaypoint >= waypoints.length) {
			currentWaypoint = 0;
		}
	}
}

function Update () {
	if(currentStatus == DEACTIVATED) {
		currentStatus 	= FLYING;
	} 
	ApplyStatus();
}

function ApplyStatus() {
	switch(currentStatus) {
			case DEACTIVATED:
				break;
			case FLYING:
				Flying();
				break;
			case DYING:
				Die();
				break;
	}
	if(currentStatus!=oldStatus) {
		Debug.Log("cambiato status da "+oldStatus+" a "+currentStatus);
		oldStatus = currentStatus;
		
	}
}

function Flying() {
	NavigateTowardsWaypoint();
}

function Die() {
	respawner.GhostDead();
	Destroy( gameObject );
	target.GetComponent("ThirdPersonStatus").AddPoints(bonusPoints);
}

// This next function responds to the "HidePlayer" message by hiding the player. 
// The message is also 'replied to' by identically-named functions in the collision-handling scripts.
// - Used by the LevelStatus script when the level completed animation is triggered.

function HideGhost()
{
	GetComponentInChildren(SkinnedMeshRenderer).enabled = false; // stop rendering the player.
	isControllable = false;	// disable player controls.
}

// This is a complementary function to the above. We don't use it in the tutorial, but it's included for
// the sake of completeness. (I like orthogonal APIs; so sue me!)

function ShowGhost()
{
	GetComponentInChildren(SkinnedMeshRenderer).enabled = true; // start rendering the player again.
	isControllable = true;	// allow player to control the character again.
}

function OnTriggerEnter(what: Collider) {
	var tagName = what.gameObject.tag;
	Debug.Log ("Ghost OnTriggerEnter: "+tagName );
	if(tagName=="spear" || tagName=="pugnale") {
		Destroy(what.gameObject);
		health--;
		if(health == 0) {
			Die();
		}
	}
}

function OnControllerColliderHit (hit : ControllerColliderHit )
{
//	Debug.DrawRay(hit.point, hit.normal);
	if (hit.moveDirection.y > 0.01) 
		return;
}

function GetSpeed () {
	return moveSpeed;
}


// Require a character controller to be attached to the same game object
@script RequireComponent(CharacterController)
@script AddComponentMenu("Ghost/Ghost")