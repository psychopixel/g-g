var target : Transform;
var home: Transform;

var bonusPoints = 2000;

// The speed when walking
var walkSpeed:float 					= 1;
var chargeSpeed:float 				= 6;
var chargeStartTime:float				= 0.0;
var chargeDuration:float				= 3.0;
var moveOnGroundStartTime:float 	= 0;
var moveOnGroundDuration:float 	= 5;
var jumpAttackStartTime:float		= 0.0;
var jumpAttackDuration:float			= 1;
var jumpAttackDeltaY:float			= 2;
var jumpAttackMaxLength:float		= 10;
var shotPercentual:float				= 0.005;

var isCharging:boolean				= false;
var isJumping:boolean					= false;
var activationDistance					= 14.0;
var rotateSpeed 							= 500.0;

private var jumpAttackTargetPosition:Vector3;
private var jumpAttackStartPosition:Vector3;
private var maxFollowX:float 		= -238;
var maxHealth:int 						= 10;
var health:int								= maxHealth;

private var DEACTIVATED:String				= "DEACTIVATED";
private var JUMP_ATTACK:String				= "JUMP_ATTACK";
private var CHARGING:String				 		= "CHARGING";
private var MOVING_ON_GROUND:String 		= "MOVING_ON_GROUND";
private var DYING:String 							= "DYING";

var oldStatus:String			= DEACTIVATED;
var currentStatus:String 	= DEACTIVATED;

private var rndPerc:float				= 0.0;

// The gravity for the character
var gravity = 20.0;

// The current move direction in x-z
private var moveDirection = Vector3.zero;

// The current vertical speed
private var verticalSpeed = 0.0;

// The current x-z move speed
private var moveSpeed = walkSpeed;

// The last collision flags returned from controller.Move
private var collisionFlags : CollisionFlags; 

function Awake () {
	moveDirection = transform.TransformDirection(Vector3.forward);
	if(!target) {
		target = GameObject.Find("Player").transform;
	}
}


function Start ()
{
	// By default loop all animations
	animation.wrapMode = WrapMode.Loop;

	animation["charge"].layer = -1;
	animation["walk"].layer = -1;
	animation["jump"].layer = -1;
	animation["jump"].wrapMode = WrapMode.Once;
	animation.SyncLayer(-1);
	
	// We are in full control here - don't let any other animations play when we start
	animation.Stop();
}

function Update () {
		ApplyStatus();
}

function ApplyStatus() {
	switch(currentStatus) {
			case DEACTIVATED:
				var dist = Vector3.Distance(target.position, transform.position);
				if(dist <= activationDistance) {
					currentStatus 	= MOVING_ON_GROUND;
					animation.CrossFade("walk");
				}
				break;
			case JUMP_ATTACK:
				JumpAttack();
				break;
			case CHARGING:
				Charging();
				break;
			case MOVING_ON_GROUND:
				MovingOnGround();
				break;
			case DYING:
				Die();
				break;
	}
	if(currentStatus!=oldStatus) {
		Debug.Log("cambiato status da "+oldStatus+" a "+currentStatus);
		oldStatus = currentStatus;
		
	}
}

function Charging() {
	if(chargeStartTime == 0) {
		chargeStartTime = Time.time;
		animation.CrossFade("charge");
	} else if (Time.time > chargeStartTime + chargeDuration ) {
		chargeStartTime = 0;
		currentStatus = MOVING_ON_GROUND;
	}
	// Apply gravity
	ApplyGravity ();
	if(target.position.x<maxFollowX) {
		angle = RotateTowardsPosition(target.position, rotateSpeed);
		angle = Mathf.Abs(RotateTowardsPosition(target.position, rotateSpeed));
	} else {
		angle = RotateTowardsPosition(home.position, rotateSpeed);
		angle = Mathf.Abs(RotateTowardsPosition(home.position, rotateSpeed));
	}
	move = Mathf.Clamp01((90 - angle) / 90);
	direction = transform.TransformDirection(Vector3.forward * chargeSpeed * move);
	var controller : CharacterController = GetComponent(CharacterController);
	controller.SimpleMove(direction);
}

function JumpAttack() {
	if(jumpAttackStartTime == 0) {
		animation.Stop();
		animation.Play("jump");
		jumpAttackStartTime = Time.time;
		jumpAttackTargetPosition = target.transform.position;
		jumpAttackStartPosition = transform.position;
		if(Vector3.Distance(jumpAttackTargetPosition,jumpAttackStartPosition)> jumpAttackMaxLength) {
			jumpAttackTargetPosition = (Vector3.Normalize((jumpAttackTargetPosition-jumpAttackStartPosition))*jumpAttackMaxLength)+jumpAttackStartPosition;
		}
		rotateToTarget();
	} else if (Time.time > jumpAttackStartTime + jumpAttackDuration ) {
		jumpAttackStartTime = 0;
		currentStatus = MOVING_ON_GROUND;
	} else {
		var timePerc:float = (Time.time - jumpAttackStartTime)/ jumpAttackDuration;
		var jumpPosition = Vector3.Lerp(jumpAttackStartPosition, jumpAttackTargetPosition, timePerc);
		var newY:float;
		if(timePerc<=0.5) {
			newY = jumpAttackStartPosition.y + (jumpAttackDeltaY * timePerc * 2);
		} else {
			newY = jumpAttackStartPosition.y + (jumpAttackDeltaY * Mathf.Abs(1-timePerc) * 2);
		}
		jumpPosition.y = newY;
		transform.position = jumpPosition;
	}
}

function MovingOnGround() {
	if(moveOnGroundStartTime == 0) {
		moveOnGroundStartTime = Time.time;
		animation.CrossFade("walk");
	} else if (Time.time > moveOnGroundStartTime + moveOnGroundDuration ) {
		rndPerc = Random.value;
		if(rndPerc>=0.3 && target.position.x<maxFollowX) {
			moveOnGroundStartTime = 0;
			rndPerc = Random.value;
			if(rndPerc>=0.5) {
				jumpAttackStartTime = 0;
				currentStatus = JUMP_ATTACK;
			} else {
				chargeStartTime = 0;
				currentStatus = CHARGING;
			}
		} else {
			moveOnGroundStartTime = 0;
		}
	}
	// Apply gravity
	ApplyGravity ();
	if(target.position.x<maxFollowX) {
		angle = RotateTowardsPosition(target.position, rotateSpeed);
		angle = Mathf.Abs(RotateTowardsPosition(target.position, rotateSpeed));
	} else {
		angle = RotateTowardsPosition(home.position, rotateSpeed);
		angle = Mathf.Abs(RotateTowardsPosition(home.position, rotateSpeed));
		moveOnGroundStartTime = 0;
		chargeStartTime = 0;
		currentStatus = CHARGING;
	}
	move = Mathf.Clamp01((90 - angle) / 90);
	direction = transform.TransformDirection(Vector3.forward * walkSpeed * move);
	var controller : CharacterController = GetComponent(CharacterController);
	controller.SimpleMove(direction);
	// sometime he shot
	rndPerc = Random.value;
	if(rndPerc <= shotPercentual) {
		Shoting();
	}
}

function Shoting() {
	Debug.Log("Shoting");
	GetComponentInChildren(BossFireGlobeLauncher).SendMessage("LaunchFireGlobe", SendMessageOptions.DontRequireReceiver);
}

// This next function responds to the "HidePlayer" message by hiding the player. 
// The message is also 'replied to' by identically-named functions in the collision-handling scripts.
// - Used by the LevelStatus script when the level completed animation is triggered.

function HideBoss()
{
	GameObject.Find("bossMesh").GetComponent(SkinnedMeshRenderer).enabled = false; // stop rendering the player.
	isControllable = false;	// disable player controls.
}

// This is a complementary function to the above. We don't use it in the tutorial, but it's included for
// the sake of completeness. (I like orthogonal APIs; so sue me!)

function ShowBoss()
{
	GameObject.Find("bossMesh").GetComponent(SkinnedMeshRenderer).enabled = true; // start rendering the player again.
	isControllable = true;	// allow player to control the character again.
}
function rotateToTarget() {
	angle = RotateTowardsPosition(target.position, rotateSpeed);
	angle = Mathf.Abs(RotateTowardsPosition(target.position, rotateSpeed));
	move = Mathf.Clamp01((90 - angle) / 90);
	direction = transform.TransformDirection(Vector3.forward * move);
	return direction;
}
function RotateTowardsPosition (targetPos : Vector3, rotateSpeed : float) : float
{
	// Compute relative point and get the angle towards it
	var relative = transform.InverseTransformPoint(targetPos);
	var angle = Mathf.Atan2 (relative.x, relative.z) * Mathf.Rad2Deg;
	// Clamp it with the max rotation speed
	var maxRotation = rotateSpeed * Time.deltaTime;
	var clampedAngle = Mathf.Clamp(angle, -maxRotation, maxRotation);
	// Rotate
	transform.Rotate(0, clampedAngle, 0);
	// Return the current angle
	return angle;
}

function ApplyGravity () {
	if(currentStatus!=JUMP_ATTACK) {
		if (IsGrounded ()) {
			verticalSpeed = 0.0;
		} else {
			verticalSpeed -= gravity * Time.deltaTime;
		}
	} else {
		verticalSpeed = 0.0;
	}
}

function OnTriggerEnter(what: Collider) {
	Debug.Log ("Boss OnTriggerEnter: "+what.gameObject.tag );
	if(what.gameObject.tag=="spear" || what.gameObject.tag=="pugnale" ) {
		Destroy(what.gameObject);
		health--;
		if(health == 0) {
			Die();
		}
	}
}

function Die() {
	Destroy( gameObject );
	target.GetComponent("ThirdPersonStatus").AddPoints(bonusPoints);
}

function OnControllerColliderHit (hit : ControllerColliderHit )
{
//	Debug.DrawRay(hit.point, hit.normal);
	if (hit.moveDirection.y > 0.01) 
		return;
}

function GetSpeed () {
	return moveSpeed;
}

function GetDirection () {
	return moveDirection;
}

function IsGrounded () {
	return (collisionFlags & CollisionFlags.CollidedBelow) != 0;
}

function Reset ()
{
	gameObject.tag = "Boss";
}
// Require a character controller to be attached to the same game object
@script RequireComponent(CharacterController)
@script AddComponentMenu("Boss/Boss")