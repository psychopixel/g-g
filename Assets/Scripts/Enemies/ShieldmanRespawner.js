var enemy: Transform;
var respwanPosition: Vector3;
var updateInterval:float= 1.0;
var maxZvariation:float = 2.5;
var maxAmpiezzaVariation:float = 1;
var maxFrequenzaVariation:float = 1;
private var lastInterval : double; // Last interval end time
private var respawnerActive: boolean = false;

function OnTriggerEnter(what: Collider) {
	var tagName = what.gameObject.tag;
	if(tagName == "Player") {
		respawnerActive = true;
		lastInterval = Time.realtimeSinceStartup;
	}
}

function OnTriggerExit(what: Collider)  {
	var tagName = what.gameObject.tag;
	if(tagName == "Player") {
		respawnerActive = false;
		lastInterval = 0.0;
	}
}


function Update () {
	if(respawnerActive) {
		var timeNow = Time.realtimeSinceStartup;
		if( timeNow > lastInterval + updateInterval ) {
			lastInterval = timeNow;
			RespawnShieldman();
		}
	} 
}

function RespawnShieldman() {
	if(enemy) {
		var modifiedRespawnPosition:Vector3 = respwanPosition;
		var rndPerc:float = Random.value - 0.5;
		modifiedRespawnPosition.z += maxZvariation * rndPerc;
		var newEnemy = Instantiate(enemy, modifiedRespawnPosition, transform.rotation);
		rndPerc = Random.value - 0.5;
		newEnemy.GetComponent("ShieldmanAnimation").ampiezza += maxAmpiezzaVariation * rndPerc;
		rndPerc = Random.value - 0.5;
		newEnemy.GetComponent("ShieldmanAnimation").frequenza += maxFrequenzaVariation * rndPerc;
	}
}