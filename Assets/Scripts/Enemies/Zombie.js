var target : Transform;

var bonusPoints = 100;

// The speed when walking
var walkSpeed = 3;

// The gravity for the character
var gravity = 20.0;

var rotateSpeed = 500.0;

// The current move direction in x-z
private var moveDirection = Vector3.zero;

// The current vertical speed
private var verticalSpeed = 0.0;

// The current x-z move speed
private var moveSpeed = walkSpeed;
private var born = false;
// The last collision flags returned from controller.Move
private var collisionFlags : CollisionFlags; 

function Awake () {
	moveDirection = transform.TransformDirection(Vector3.forward);
	if(!target) {
		target = GameObject.Find("Player").transform;
	}
}

function Start () {
	// By default loop all animations
	animation.wrapMode = WrapMode.Loop;
}

function Update () {
	
	// Apply gravity
	ApplyGravity ();
	angle = RotateTowardsPosition(target.position, rotateSpeed);
	angle = Mathf.Abs(RotateTowardsPosition(target.position, rotateSpeed));
	move = Mathf.Clamp01((90 - angle) / 90);
	direction = transform.TransformDirection(Vector3.forward * walkSpeed * move);
	var controller : CharacterController = GetComponent(CharacterController);
	controller.SimpleMove(direction);
	
}
// This next function responds to the "HidePlayer" message by hiding the player. 
// The message is also 'replied to' by identically-named functions in the collision-handling scripts.
// - Used by the LevelStatus script when the level completed animation is triggered.

function HideZombie()
{
	GameObject.Find("zombieMesh").GetComponent(SkinnedMeshRenderer).enabled = false; // stop rendering the player.
	isControllable = false;	// disable player controls.
}

// This is a complementary function to the above. We don't use it in the tutorial, but it's included for
// the sake of completeness. (I like orthogonal APIs; so sue me!)

function ShowZombie()
{
	GameObject.Find("zombieMesh").GetComponent(SkinnedMeshRenderer).enabled = true; // start rendering the player again.
	isControllable = true;	// allow player to control the character again.
}

function RotateTowardsPosition (targetPos : Vector3, rotateSpeed : float) : float
{
	// Compute relative point and get the angle towards it
	var relative = transform.InverseTransformPoint(targetPos);
	var angle = Mathf.Atan2 (relative.x, relative.z) * Mathf.Rad2Deg;
	// Clamp it with the max rotation speed
	var maxRotation = rotateSpeed * Time.deltaTime;
	var clampedAngle = Mathf.Clamp(angle, -maxRotation, maxRotation);
	// Rotate
	transform.Rotate(0, clampedAngle, 0);
	// Return the current angle
	return angle;
}

function ApplyGravity () {
	if(IsUnderground() && !born) {
		transform.position.y += 0.05;
	} else {
		if (IsGrounded ()) {
			verticalSpeed = 0.0;
		}	else {
			verticalSpeed -= gravity * Time.deltaTime;
		}
	}
}

function OnTriggerEnter(what: Collider) {
	Debug.Log ("Zombie OnTriggerEnter: "+what.gameObject.tag );
	if(what.gameObject.tag=="spear" || what.gameObject.tag=="pugnale" ) {
		Destroy( gameObject );
		Destroy(what.gameObject);
		target.GetComponent("ThirdPersonStatus").AddPoints(bonusPoints);
	}
}

function OnControllerColliderHit (hit : ControllerColliderHit )
{
//	Debug.DrawRay(hit.point, hit.normal);
	if (hit.moveDirection.y > 0.01) 
		return;
}

function GetSpeed () {
	return moveSpeed;
}

function GetDirection () {
	return moveDirection;
}

function IsGrounded () {
	return (collisionFlags & CollisionFlags.CollidedBelow) != 0;
}
function IsUnderground() {
	if(transform.position.y < -4) {
		return true;
	} else {
		born = true;
		return false;
	}
}
function Reset ()
{
	gameObject.tag = "Zombie";
}
// Require a character controller to be attached to the same game object
@script RequireComponent(CharacterController)
@script AddComponentMenu("Zombie/Zombie")


