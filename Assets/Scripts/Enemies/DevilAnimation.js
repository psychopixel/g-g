var target : Transform;

var bonusPoints = 1000;

// The speed when walking
var walkSpeed:float 					= 0.015;
var walkMultiplier:float					= 2;
var chargeStartTime:float				= 0.0;
var chargeDuration:float				= 4.0;
var moveInAirStartTime:float			= 0.0;
var moveInAirDuration:float			= 5.0;
var moveOnGroundStartTime:float 	= 0;
var moveOnGroundDuration:float 	= 5;
var killAttackDistance:float			= 0.01;
var killAttackStartTime:float			= 0.0;
var killAttackDuration:float			= 4.0;
var nearKillAttackDistance:float		= 1.0;
var nearKillAttackStartTime:float	= 0.0;
var nearKillAttackDuration:float		= 4.0;
var killAttackMultiplier:float			= 7.0;
var rotateSpeed:float					= 60.0;
var up_y:float							= -1;
var down_y:float						= -4.19;
var y_step:float							= 1;
var yMultiplier:float						= 2;
var tolerance:float						= 0.05;
var activationDistance					= 10.0;
var minimumDistance					= 2.0;

private var rndPerc:float				= 0.0;
/*
private var DEACTIVATED:int 				= 0;
private var GOING_UP:int 					= 1;
private var SHOTING:int 						= 2;
private var MOVING_IN_AIR:int 			= 3;
private var KILL_ATTACK:int				= 4;
private var NEAR_KILL_ATTACK:int		= 5;
private var GOING_DOWN:int 				= 6;
private var CHARGING:int				 		= 7;
private var MOVING_ON_GROUND:int 		= 8;
private var DYING:int 							= 9;

var oldStatus:int			= DEACTIVATED;
var currentStatus:int 	= GOING_UP;

*/
private var DEACTIVATED:String				= "DEACTIVATED";
private var GOING_UP:String 					= "GOING_UP";
private var SHOTING:String 						= "SHOTING";
private var MOVING_IN_AIR:String 			= "MOVING_IN_AIR";
private var KILL_ATTACK:String				= "KILL_ATTACK";
private var NEAR_KILL_ATTACK:String		= "NEAR_KILL_ATTACK";
private var GOING_DOWN:String 				= "GOING_DOWN";
private var CHARGING:String				 		= "CHARGING";
private var MOVING_ON_GROUND:String 		= "MOVING_ON_GROUND";
private var DYING:String 							= "DYING";

var oldStatus:String			= DEACTIVATED;
var currentStatus:String 	= GOING_UP;

private var UP:int								= 0;
private var DOWN:int							= 1;
private var STATIONARY:int					= 2;

private var versoAscensione:int;
private var posizioneAscensione:int;

var maxHealth:int = 4;
var health:int;



// The gravity for the character
var gravity = 0.0;

// The current move direction in x-z
private var moveDirection = Vector3.zero;

// The current vertical speed
private var verticalSpeed = 0.0;

// The current x-z move speed
private var moveSpeed = walkSpeed;

// The last collision flags returned from controller.Move
private var collisionFlags : CollisionFlags; 

function Awake () {
	moveDirection = transform.TransformDirection(Vector3.forward);
	if(!target) {
		target = GameObject.Find("Player").transform;
	}
}


function Start ()
{
	// By default loop all animations
	animation.wrapMode = WrapMode.Loop;

	animation["idle"].layer = -1;
	animation.SyncLayer(-1);
	
	// We are in full control here - don't let any other animations play when we start
	animation.Stop();
	animation.Play("idle");
	
	oldStatus		= DEACTIVATED;
	currentStatus 	= GOING_UP;
	health = maxHealth;
	posizioneAscensione = DOWN;
	versoAscensione = UP;
}

function Update () {
	var dist = Vector3.Distance(target.position, transform.position);
	if(dist <= activationDistance || currentStatus == DYING) {
		ApplyStatus();
	} else {
	
	}
	
}

function ApplyStatus() {
	
	switch(currentStatus) {
			case DEACTIVATED:
				break;
			case GOING_UP:
				GoingUp();
				break;
			case SHOTING:
				Shoting();
				break;
			case MOVING_IN_AIR:
				MoveInAir();
				break;
			case KILL_ATTACK:
				KillAttack();
				break;
			case NEAR_KILL_ATTACK:
				NearKillAttack();
				break;
			case GOING_DOWN:
				GoingDown();
				break;
			case CHARGING:
				Charging();
				break;
			case MOVING_ON_GROUND:
				MovingOnGround();
				break;
			case DYING:
				Die();
				break;
	}
	if(currentStatus!=oldStatus) {
		Debug.Log("cambiato status da "+oldStatus+" a "+currentStatus);
		oldStatus = currentStatus;
		
	}
}
function GoingUp() {
	if(oldStatus != currentStatus) {
		animation.CrossFade("wake");
		yield WaitForSeconds(0.15);
		animation.CrossFade("flyUp");
		yield WaitForSeconds(0.2);
		animation.CrossFade("fly");
	}
	versoAscensione = UP;
	rotateToTarget();
	if(transform.position.y - tolerance <= up_y) {
		transform.position.y += (y_step * Time.deltaTime) * yMultiplier;
	} else if(transform.position.y + tolerance > up_y) {
		transform.position.y = up_y;
		currentStatus = SHOTING;
		posizioneAscensione = UP;
		versoAscensione = STATIONARY;
	}
}
function GoingDown() {
	versoAscensione = DOWN;
	Follow();
	if(transform.position.y >= down_y) {
		transform.position.y -= (y_step * Time.deltaTime)  * yMultiplier;
	} else if(transform.position.y < down_y) {
		transform.position.y = down_y;
		currentStatus = MOVING_ON_GROUND;
		posizioneAscensione = DOWN;
		versoAscensione = STATIONARY;
	}
}
function Shoting() {
	Debug.Log("Shoting");
	GameObject.Find("devilFireBallLauncher").GetComponent("DevilFireGlobeLauncher").SendMessage("LaunchFireGlobe", SendMessageOptions.DontRequireReceiver);

	if(posizioneAscensione==UP) {
		currentStatus = MOVING_IN_AIR;
	} else {
		currentStatus = MOVING_ON_GROUND;
	}

}
function KillAttack() {
	Debug.Log("KillAttack");
	
	if(killAttackStartTime == 0) {
		versoAscensione = DOWN;
		killAttackStartTime = Time.time;
		Debug.Log("killAttackStartTime: "+moveInAirStartTime);
		rotateToTarget();
	} else if (Time.time > killAttackStartTime + killAttackDuration) {
		killAttackStartTime = 0;
		currentStatus = GOING_UP;
	} else {
		var dist = Vector3.Distance(target.position, transform.position);
		if(dist <= killAttackDistance) {
			versoAscensione = UP;
			currentStatus = GOING_UP;
		} else {
			transform.LookAt(target);
			Follow();
		}
	}
}
function NearKillAttack() {
	Debug.Log("NearKillAttack");
	if(nearKillAttackStartTime == 0) {
		versoAscensione = DOWN;
		nearKillAttackStartTime = Time.time;
		Debug.Log("nearKillAttackStartTime: "+moveInAirStartTime);
		rotateToTarget();
	} else if (Time.time > nearKillAttackStartTime + nearKillAttackDuration) {
		nearKillAttackStartTime = 0;
		currentStatus = GOING_UP;
	} else {		
		var dist = Vector3.Distance(target.position, transform.position);
		if(dist <= nearKillAttackDistance) {
			versoAscensione = UP;
			currentStatus = GOING_UP;
		} else {
			Follow();
		}
	}
}
function MoveInAir() {
	if(moveInAirStartTime == 0) {
		moveInAirStartTime = Time.time;
		Debug.Log("moveInAirStartTime: "+moveInAirStartTime);
		rotateToTarget();
	} else if (Time.time > moveInAirStartTime + moveInAirDuration) {
		rndPerc = Random.value;
		moveInAirStartTime = 0;
		if(rndPerc<=0.2) {
			currentStatus = SHOTING;
		} else if(rndPerc<=0.7) {
			rndPerc = Random.value;
			if(rndPerc<=0.7) {
				currentStatus = KILL_ATTACK;
			} else {
				currentStatus = NEAR_KILL_ATTACK;
			}
		} else {
			currentStatus = GOING_DOWN;
		}
	} else {
		rotateToTarget();
		Follow();
	}

}
function Charging() {
	if(chargeStartTime == 0) {
		chargeStartTime = Time.time;
	} else if (Time.time > chargeStartTime + chargeDuration) {
		rndPerc = Random.value;
		chargeStartTime = 0;
		if(rndPerc<=0.2) {
			currentStatus = MOVING_ON_GROUND;
		} else {
			currentStatus = GOING_UP;
		}
	}
	Follow();
}
function MovingOnGround() {
	animation.CrossFade("walk");
	if(moveOnGroundStartTime == 0) {
		moveOnGroundStartTime = Time.time;
	} else if (Time.time > moveOnGroundStartTime + moveOnGroundDuration ) {
		rndPerc = Random.value;
		moveOnGroundStartTime = 0;
		if(rndPerc<=0.3) {
			currentStatus = SHOTING;
		} else if(rndPerc<=0.8) {
			animation.CrossFade("fly");
			currentStatus = KILL_ATTACK;
		} else {
			animation.CrossFade("fly");
			currentStatus = GOING_UP;
		}
	}
	Follow();
}
function rotateToTarget() {
	angle = RotateTowardsPosition(target.position, rotateSpeed);
	angle = Mathf.Abs(RotateTowardsPosition(target.position, rotateSpeed));
	move = Mathf.Clamp01((90 - angle) / 90);
	direction = transform.TransformDirection(Vector3.forward * move);
	return direction;
}
function Follow() {
	direction = rotateToTarget();
	
	var dist = Vector3.Distance(target.position, transform.position);
	var controller : CharacterController = GetComponent(CharacterController);
	controller.Move(direction * walkSpeed);
}
function Die() {
	Destroy( gameObject );
	target.GetComponent("ThirdPersonStatus").AddPoints(bonusPoints);
}

// This next function responds to the "HidePlayer" message by hiding the player. 
// The message is also 'replied to' by identically-named functions in the collision-handling scripts.
// - Used by the LevelStatus script when the level completed animation is triggered.

function HideDevil()
{
	GameObject.Find("devilMesh").GetComponent(SkinnedMeshRenderer).enabled = false; // stop rendering the player.
	isControllable = false;	// disable player controls.
}

// This is a complementary function to the above. We don't use it in the tutorial, but it's included for
// the sake of completeness. (I like orthogonal APIs; so sue me!)

function ShowDevil()
{
	GameObject.Find("devilMesh").GetComponent(SkinnedMeshRenderer).enabled = true; // start rendering the player again.
	isControllable = true;	// allow player to control the character again.
}

function RotateTowardsPosition (targetPos : Vector3, rotateSpeed : float) : float
{
	// Compute relative point and get the angle towards it
	var relative = transform.InverseTransformPoint(targetPos);
	var angle = Mathf.Atan2 (relative.x, relative.z) * Mathf.Rad2Deg;
	// Clamp it with the max rotation speed
	var maxRotation = rotateSpeed * Time.deltaTime;
	var clampedAngle = Mathf.Clamp(angle, -maxRotation, maxRotation);
	// Rotate
	transform.Rotate(0, clampedAngle, 0);
	// Return the current angle
	return angle;
}

function ApplyGravity () {

	if (IsGrounded ())
			verticalSpeed = 0.0;
		else
			verticalSpeed -= gravity * Time.deltaTime;
}

function OnTriggerEnter(what: Collider) {
	var tagName = what.gameObject.tag;
	Debug.Log ("Devil OnTriggerEnter: "+tagName );
	if(tagName=="spear" || tagName=="pugnale" ) {
		Destroy(what.gameObject);
		health--;
		if(health == 0) {
			Die();
		} else {
			if(versoAscensione==UP) {
				currentStatus = MOVING_IN_AIR;
			} else {
				currentStatus = MOVING_ON_GROUND;
			}
		}
		ApplyStatus();
	}
}

function OnControllerColliderHit (hit : ControllerColliderHit )
{
//	Debug.DrawRay(hit.point, hit.normal);
	if (hit.moveDirection.y > 0.01) 
		return;
}

function GetSpeed () {
	return moveSpeed;
}

function GetDirection () {
	return moveDirection;
}

function IsGrounded () {
	return (collisionFlags & CollisionFlags.CollidedBelow) != 0;
}

function Reset ()
{
	//gameObject.tag = "Devil";
}
// Require a character controller to be attached to the same game object
@script RequireComponent(CharacterController)
@script AddComponentMenu("Devil/Devil")