var enemy: Transform;
var respwanPosition: Vector3;
var updateInterval:float= 1.0;
var pathToFollow:GameObject;
var maxGhosts:int = 5;
var currentGhosts:int = 0;
private var lastInterval : double; // Last interval end time
private var respawnerActive: boolean = false;

function OnTriggerEnter(what: Collider) {
	var tagName = what.gameObject.tag;
	if(tagName == "Player") {
		respawnerActive = true;
		lastInterval = Time.realtimeSinceStartup;
	}
}

function OnTriggerExit(what: Collider)  {
	var tagName = what.gameObject.tag;
	if(tagName == "Player") {
		respawnerActive = false;
		lastInterval = 0.0;
	}
}


function Update () {
	if(respawnerActive) {
		var timeNow = Time.realtimeSinceStartup;
		if( timeNow > lastInterval + updateInterval ) {
			lastInterval = timeNow;
			RespawnGhost();
		}
	} 
}

function GhostDead() {
	currentGhosts--;
}

function RespawnGhost() {
	if(currentGhosts<maxGhosts) {
		if(enemy) {
			var newEnemy = Instantiate(enemy, respwanPosition, transform.rotation);
			newEnemy.GetComponent("GhostAnimation").pathToFollow = pathToFollow;
			newEnemy.GetComponent("GhostAnimation").respawner = this;
			currentGhosts++;
		}
	}
}