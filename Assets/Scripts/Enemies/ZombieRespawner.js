var enemy: Transform;
var terra:GameObject;
var respwanPosition: Vector3;
var updateInterval = 10;
private var lastInterval : double; // Last interval end time
private var respawnerActive: boolean = false;

function Awake() {
	
}
function Start() {
	terra.animation.Stop();
	terra.animation.Play("close");
}
function OnTriggerEnter(what: Collider) {
	var tagName = what.gameObject.tag;
	if(tagName == "Player") {
		respawnerActive = true;
		lastInterval = Time.realtimeSinceStartup;
	}
}

function OnTriggerExit(what: Collider)  {
	var tagName = what.gameObject.tag;
	if(tagName == "Player") {
		respawnerActive = false;
		lastInterval = 0.0;
	}
}


function Update () {
	if(respawnerActive) {
		var timeNow = Time.realtimeSinceStartup;
		if( timeNow > lastInterval + updateInterval ) {
			lastInterval = timeNow;
			RespawnZombie();
		}
	} 
}

function RespawnZombie() {
	if(enemy) {
		terra.animation.Stop();
		terra.animation.Play("open");
		yield WaitForSeconds(1);
		terra.animation.Stop();
		terra.animation.Play("idle");
		var newEnemy = Instantiate(enemy, respwanPosition, Quaternion.identity);
		yield WaitForSeconds(1.125);
		terra.animation.Stop();
		terra.animation.Play("close");
	}
}