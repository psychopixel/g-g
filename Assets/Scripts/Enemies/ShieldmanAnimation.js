var bonusPoints = 200;
// The speed when flying
var flySpeed:float 						= 6;
var flyStartTime:float					= 0.0;
var flyDuration:float					= 35.0;
//var activationDistance					= 10.0;

private var startX:float				= 0.0;
private var initialZ:float				= 0.0;

var ampiezza:float						= 2;
var frequenza:float						= 4;
var spostamentoX:float				= 0;
var spostamentoY:float				= 0;

var maxHealth:int 						= 1;
var health:int								= maxHealth;

private var DEACTIVATED:String				= "DEACTIVATED";
private var FLYING:String 						= "FLYING";
private var DYING:String 							= "DYING";

var oldStatus:String			= DEACTIVATED;
var currentStatus:String 	= FLYING;

// The last collision flags returned from controller.Move
private var collisionFlags : CollisionFlags; 

function Awake () {
	currentStatus = FLYING;
	initialZ = transform.position.z;
}


function Start ()
{
	// By default loop all animations
	animation.wrapMode = WrapMode.Loop;

	animation["fly"].layer = -1;
	animation.SyncLayer(-1);
	
	// We are in full control here - don't let any other animations play when we start
	animation.Stop();
	animation.Play("fly");
}

function Update () {
		ApplyStatus();
}

function ApplyStatus() {
	switch(currentStatus) {
			case DEACTIVATED:
				break;
			case FLYING:
				Fly();
				break;
			case DYING:
				Die();
				break;
	}
	if(currentStatus!=oldStatus) {
		oldStatus = currentStatus;
		
	}
}

function Fly() {
	if(flyStartTime == 0) {
		flyStartTime = Time.time;
	} else {
		if (transform.position.x > -130) {
			Destroy( gameObject );
		} else {
			transform.position.x += flySpeed  * Time.deltaTime;
			startX += flySpeed * Time.deltaTime;
			transform.position.y = ampiezza * Mathf.Sin((frequenza * startX) + spostamentoX) + spostamentoY;
			transform.position.z = Mathf.Sin(startX) * 4 + initialZ;
		}
	}
}

// This next function responds to the "HidePlayer" message by hiding the player. 
// The message is also 'replied to' by identically-named functions in the collision-handling scripts.
// - Used by the LevelStatus script when the level completed animation is triggered.

function HideShieldman()
{
	GameObject.Find("shieldman").GetComponent(SkinnedMeshRenderer).enabled = false; // stop rendering the player.
	isControllable = false;	// disable player controls.
}

// This is a complementary function to the above. We don't use it in the tutorial, but it's included for
// the sake of completeness. (I like orthogonal APIs; so sue me!)

function ShowShieldman()
{
	GameObject.Find("shieldman").GetComponent(SkinnedMeshRenderer).enabled = true; // start rendering the player again.
	isControllable = true;	// allow player to control the character again.
}

function OnTriggerEnter(what: Collider) {
	Debug.Log ("Shieldman OnTriggerEnter: "+what.gameObject.tag );
	if(what.gameObject.tag=="spear" || what.gameObject.tag=="pugnale" ) {
		Destroy(what.gameObject);
		health--;
		if(health == 0) {
			Die();
		}
	}
}

function Die() {
	Destroy( gameObject );
	GameObject.Find("Player").GetComponent("ThirdPersonStatus").AddPoints(bonusPoints);
}

function OnControllerColliderHit (hit : ControllerColliderHit )
{
//	Debug.DrawRay(hit.point, hit.normal);
	if (hit.moveDirection.y > 0.01) 
		return;
}


function Reset ()
{
	gameObject.tag = "shieldman";
}
// Require a character controller to be attached to the same game object
@script RequireComponent(CharacterController)
@script AddComponentMenu("Shieldman/Shieldman")