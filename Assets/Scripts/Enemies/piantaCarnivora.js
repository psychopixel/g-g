var target : Transform;
var maxDistance:int = 25;
var updateInterval = 3;
private var lastInterval : double; // Last interval end time
private var plantActive: boolean = false;

var bonusPoints = 300;

var rotateSpeed = 500.0;

function Awake () {
	if(!target) {
		target = GameObject.Find("Player").transform;
	}
}

function Start ()
{
	// By default loop all animations
	//animation.wrapMode = WrapMode.Loop;
	animation["shot"].layer = -1;
	
	animation.Stop();
	animation.Play("shot");
}

function Update () {
	angle = RotateTowardsPosition(target.position, rotateSpeed);
	angle = Mathf.Abs(RotateTowardsPosition(target.position, rotateSpeed));
	var dist = Vector3.Distance(target.transform.position, transform.position);
	var timeNow = Time.realtimeSinceStartup;
	if(dist <= maxDistance) {
		if(!plantActive) {
			Debug.Log("PIANTA ATTIVATA");
			lastInterval = timeNow;
		}
		plantActive=true;
	} else {
		lastInterval = 0;
		plantActive=false;
	}
	if(plantActive) {
		if( timeNow > lastInterval + updateInterval ) {
			Debug.Log("FIRE");
			lastInterval = timeNow;
			launch();
		}
	}
}
function launch() {
	animation.Rewind("shot");
	animation.Play("shot");
	yield WaitForSeconds(1);
	GetComponentInChildren(FireGlobeLauncher).SendMessage("LaunchFireGlobe", SendMessageOptions.DontRequireReceiver);
}
function RotateTowardsPosition (targetPos : Vector3, rotateSpeed : float) : float
{
	// Compute relative point and get the angle towards it
	var relative = transform.InverseTransformPoint(targetPos);
	var angle = Mathf.Atan2 (relative.x, relative.z) * Mathf.Rad2Deg;
	// Clamp it with the max rotation speed
	var maxRotation = rotateSpeed * Time.deltaTime;
	var clampedAngle = Mathf.Clamp(angle, -maxRotation, maxRotation);
	// Rotate
	transform.Rotate(0, clampedAngle, 0);
	// Return the current angle
	return angle;
}

function OnTriggerEnter(what: Collider) {
	Debug.Log ("Pianta OnTriggerEnter: "+what.gameObject.tag );
	if(what.gameObject.tag=="spear" || what.gameObject.tag=="pugnale" ) {
		Destroy( gameObject );
		Destroy(what.gameObject);
		target.GetComponent("ThirdPersonStatus").AddPoints(bonusPoints);
	}
}

@script AddComponentMenu("PiantaCarnivora/PiantaCarnivora")