var bonusPoints = 150;

var coinAudio : AudioClip;

function OnTriggerEnter(what: Collider) {
	Debug.Log ("Coin OnTriggerEnter: "+what.gameObject.tag );
	if(what.gameObject.tag=="Player") {
	what.gameObject.GetComponent("ThirdPersonStatus").AddPoints(bonusPoints);
		AudioSource.PlayClipAtPoint(coinAudio, transform.position);	// play the 'coin taken' sound.
		yield WaitForSeconds (0.3);
		Destroy( gameObject );
		
	}
}