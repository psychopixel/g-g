var bonusPoints = 200;

var armaturinaAudio : AudioClip;

function OnTriggerEnter(what: Collider) {
	Debug.Log ("Armaturina OnTriggerEnter: "+what.gameObject.tag );
	if(what.gameObject.tag=="Player") {
	what.gameObject.GetComponent("ThirdPersonStatus").AddPoints(bonusPoints);
		AudioSource.PlayClipAtPoint(armaturinaAudio, transform.position);	// play the 'coin taken' sound.
		yield WaitForSeconds (0.3);
		Destroy( gameObject );
	}
}