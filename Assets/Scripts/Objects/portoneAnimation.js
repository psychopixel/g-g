var target:Transform;
var openDistance:float = 4;
private var isOpening:boolean = false;
function Start() {
	// By default loop all animations
	animation.wrapMode = WrapMode.Once;

	animation["portoneOpen"].layer = -1;
	animation["portoneClose"].layer = -1;
	animation.SyncLayer(-1);
	animation.Stop();
}

function Update () {
	var dist = Vector3.Distance(target.position, transform.position);
	if(dist <= openDistance ) {
		if(!isOpening) {
			var boss = GameObject.Find("Boss");
			if(boss==null) {
				isOpening = true;
				animation.Play("portoneOpen");
			}
		}
	} else {
		if(isOpening) {
			isOpening = false;
			animation.CrossFade("portoneClose");
		}
	}
}