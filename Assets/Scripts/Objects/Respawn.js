/*
Respawn: Allows players to respawn to this point in the level, effectively saving their progress.

The Respawn object has three main states and one interim state: Inactive, Active and Respawn, plus Triggered.

- Inactive: Player hasn't reached this point and the player will not respawn here.

- Active: Player has touched this respawn point, so the player will respawn here.

- Respawn: Player is respawning at this respawn point.

Each state has its own visual effect(s).

Respawn objects also require a simple collider, so the player can activate them. The collider is set as a trigger.

*/

var initialRespawn : Respawn;	// set this to the initial respawn point for the level.

var RespawnState = 0;

// Sound effects:
var SFXPlayerRespawn: AudioClip;

var SFXVolume: float;	// volume for one-shot sounds.


// The currently active respawn point. Static, so all instances of this script will share this variable.
static var currentRespawn : Respawn;

function Start()
{	
	// Get some of the objects we need later.
	// This is often done in a script's Start function. That way, we've got all our initialization code in one place, 
	// And can simply count on the code being fine.

	RespawnState = 0;
	
	// Assign the respawn point to be this one - Since the player is positioned on top of a respawn point, it will come in and overwrite it.
	// This is just to make sure that we always have a respawn point.
	currentRespawn = initialRespawn;

}

function OnTriggerEnter(what: Collider) {
	var tagName = what.gameObject.tag;
	if(tagName == "Player") {
		if (currentRespawn != this )		// make sure we're not respawning or re-activating an already active pad!
		{
		
			// Set the current respawn point to be us and make it visible.
			currentRespawn = this;

		}
	}
}

function FireEffect () {

	if (SFXPlayerRespawn)
	{	// if we have a 'player is respawning' sound effect, play it now.
		AudioSource.PlayClipAtPoint(SFXPlayerRespawn, transform.position, SFXVolume);
	}
	
	yield WaitForSeconds (1);

}
