var minX:float = -133.8;
var minZ:float = 85;
var maxX:float = -140.5;
var maxZ:float = 80;
var step:float = 1;
var ampiezza:float = 1.8;
var frequenza:float = 1.9;

private var currentX:float;

function Awake() {
	transform.position = Vector3(minX, transform.position.y, minZ);
	currentX = transform.position.x;
}

function Update () {
	var newZ:float = ampiezza * Mathf.Sin(frequenza * currentX)+(maxZ+(minZ-maxZ));
	transform.position.z = newZ;
	transform.position.x = currentX;
	currentX += step * Time.deltaTime;
	if(step<= 0) {
		if(currentX<=maxX) {
			step *= -1;
		}
	} else {
		if(currentX>=minX) {
			step *= -1;
		}
	}
}

function OnTriggerEnter(other: Collider) {
	if (other.GetComponent (ThirdPersonStatus))
	{
		GameObject.Find("Player").transform.parent = transform;
	}
}

function OnTriggerExit (other: Collider) {
	if (other.GetComponent (ThirdPersonStatus))
	{
		GameObject.Find("Player").transform.parent = null;
	}
}