var bonusPoints = 300;

var moneyAudio : AudioClip;

function OnTriggerEnter(what: Collider) {
	Debug.Log ("MoneyBag OnTriggerEnter: "+what.gameObject.tag );
	if(what.gameObject.tag=="Player") {
	what.gameObject.GetComponent("ThirdPersonStatus").AddPoints(bonusPoints);
		AudioSource.PlayClipAtPoint(moneyAudio, transform.position);	// play the 'coin taken' sound.
		yield WaitForSeconds (0.3);
		Destroy( gameObject );
	}
}