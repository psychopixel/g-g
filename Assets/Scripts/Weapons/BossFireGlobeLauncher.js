var projectile : Rigidbody;
var target:Transform;
var speed:float = 10;

function Update () {

}

function LaunchFireGlobe() {
	var targetModPos:Vector3 = new Vector3(target.position.x, target.position.y+0.4, target.position.z);
	transform.LookAt(targetModPos);
	var instantiatedProjectile : Rigidbody = Instantiate(projectile, transform.position, Quaternion.identity);
	instantiatedProjectile.velocity = transform.TransformDirection( Vector3.forward * speed);
}