private var initialPosition: Vector3;
var maxDistance:int = 10;

function Awake() {
	rigidbody.velocity = Vector3.zero;
}

function setInitialPosition() {
	initialPosition = transform.position;
}

function OnTriggerEnter(what: Collider) {
	if(what.gameObject.tag=="weaponKiller") {
		Destroy( gameObject );
	}
}

function OnTriggerStay(what: Collider) {

	
}

function Update () {
	if(rigidbody.velocity != Vector3.zero) {
		var dist = Vector3.Distance(initialPosition, transform.position);
		if(dist > maxDistance) {
			Destroy( gameObject );
		}
	}
}