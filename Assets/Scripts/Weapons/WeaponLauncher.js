var projectile : Rigidbody;
// The minimum time between shot
var minShotInterval:float = 0.66;
var speed = 20;

function Update () {

}

function LaunchSpear() {
	Debug.Log ("LaunchSpear");
	var rotation : Quaternion = Quaternion.identity;
	if(projectile.gameObject.tag=="spear") {
		rotation = Quaternion.Euler(0, 90, 270);
	} else if (projectile.gameObject.tag=="pugnale") {
		rotation = Quaternion.Euler(0, 90, 0);
	}
	var newSpeed = speed;
	if (projectile.gameObject.tag=="pugnale") {
		newSpeed*=2;
	}
	var newRotation = Quaternion.Euler(Vector3(rotation.eulerAngles.x+transform.rotation.eulerAngles.x, rotation.eulerAngles.y+transform.rotation.eulerAngles.y, rotation.eulerAngles.z+transform.rotation.eulerAngles.z));
	var instantiatedProjectile : Rigidbody = Instantiate(projectile, transform.position, newRotation );
	instantiatedProjectile.velocity = transform.TransformDirection( Vector3( 0, 0, speed ) );
	Physics.IgnoreCollision( instantiatedProjectile. collider, transform.root.collider );
}

function GetMinInterval() {
	if(projectile.gameObject.tag=="spear") {
		return minShotInterval;
	}  else if (projectile.gameObject.tag=="pugnale") {
		return minShotInterval * 0.5;
	}
}