var weapon:Rigidbody;
var speed:float;
var minShotInterval:float				= 3;

private var  instantiatedProjectile : Rigidbody;
private var lastShotTime:float		= 0;

function Awake() {
	instantiatedProjectile = Instantiate(weapon, transform.position, transform.rotation);
	instantiatedProjectile.gameObject.transform.parent = transform;
}

function Update () {
	if( Time.time >= lastShotTime + minShotInterval && instantiatedProjectile==null) {
		instantiatedProjectile = Instantiate(weapon, transform.position, transform.rotation);
		instantiatedProjectile.gameObject.transform.parent = transform;
	}
	// bit shift the index of the layer to get a bit mask
	var layerMask = 1 << 8;
	// Does the ray intersect any objects which are in the player layer.
	var fwd = transform.TransformDirection (Vector3.forward);
	var dwn = transform.TransformDirection (Vector3(0,1,0));
	if (Physics.Raycast (transform.position, fwd, 10, layerMask)) {
		Debug.Log("Raycast intersect");
		Shot();
	}
	if (Physics.Raycast (transform.position, dwn, 10, layerMask)) {
		Debug.Log("Raycast intersect down");
		ShotDown();
	}
}

function Shot() {
	if( Time.time > lastShotTime + minShotInterval) {
		lastShotTime = Time.time;
		Debug.Log("GHOSTSPEARLAUNCHER SHOT!!!");
		instantiatedProjectile.gameObject.GetComponent(LanciaGhost).setInitialPosition();
		instantiatedProjectile.gameObject.rigidbody.velocity = instantiatedProjectile.transform.TransformDirection( Vector3.forward * speed);
		instantiatedProjectile.gameObject.transform.parent = null;
	}
}

function ShotDown() {
	if( Time.time > lastShotTime + minShotInterval) {
		lastShotTime = Time.time;
		Debug.Log("GHOSTSPEARLAUNCHER SHOT DOWN!!!");
		instantiatedProjectile.gameObject.transform.parent = null;
		var newV:Vector3 = transform.position;
		newV.y -= 5;
		instantiatedProjectile.transform.LookAt(newV);
		instantiatedProjectile.gameObject.GetComponent(LanciaGhost).setInitialPosition();
		instantiatedProjectile.gameObject.rigidbody.velocity = instantiatedProjectile.transform.TransformDirection( Vector3.forward * speed);
	}
}

function OnDrawGizmos() {
	// Draws a 10 meter long red line in front of the object
	Gizmos.color = Color.red;
	var direction : Vector3 = transform.TransformDirection (Vector3.forward) * 10;
	Gizmos.DrawRay (transform.position, direction);
	// Draws a 10 meter long red line in front of the object
	Gizmos.color = Color.green;
	var directionDown : Vector3  = transform.TransformDirection (Vector3(0,1,0)) * 10;
	Gizmos.DrawRay (transform.position, directionDown);
}

@script AddComponentMenu("Ghost/GhostSpearLauncher")