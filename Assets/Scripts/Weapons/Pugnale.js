private var initialPosition: Vector3;
var maxDistance:int = 11;

function Awake() {
	initialPosition = transform.position;
}

function OnTriggerEnter(what: Collider) {
	if(what.gameObject.tag=="weaponKiller") {
		Destroy( gameObject );
	}
}

function OnTriggerStay(what: Collider) {

	
}

function Update () {
	var dist = Vector3.Distance(initialPosition, transform.position);
	if(dist > maxDistance) {
		Destroy( gameObject );
	}
}